@uref{https://github.com/ocapn/ocapn, OCapN} stands for ``Object
Capability Network'' and is a suite of protocols for communication
between distributed, networked objects.  In other words, it's how
Goblins actors can talk to actors running on another computer.

OCapN uses the @uref{http://erights.org/elib/distrib/captp/index.html,
Capability Transport Protocol (CapTP)} as the core protocol for
enabling distributed programming in a mutually suspicious network.  It
originated in the @uref{http://www.erights.org/, E programming
language} but has since appeared in @uref{https://capnproto.org/,
Cap'N Proto}, @uref{http://waterken.sourceforge.net/, Waterken},
@uref{https://agoric.com/, Agoric}'s
@uref{https://github.com/Agoric/agoric-sdk/tree/master/packages/SwingSet,
SwingSet}, and now Goblins.

@strong{CAUTION}: CapTP support is still in its early days.  Please do
not rely on the protocol being stable at this time.

@menu
* CapTP The Capability Transport Protocol::
* Launching a Tor daemon for Goblins::
* Example Two Goblins programs chatting over CapTP via Tor::
* Using the CapTP API::
* Netlayers::
@end menu

@node CapTP The Capability Transport Protocol
@section CapTP: The Capability Transport Protocol

OCapN's CapTP has an abstract ``netlayer'' interface, allowing
communication over many different protocols ranging from Tor Onion
Services to IBC to I2P to libp2p to perhaps carrier pigeons with
backpacks full of encrypted MicroSD cards.  Currently, Goblins only
supports the Tor Onion Services netlayer.

Benefits of CapTP:

@itemize
@item
Invocation of remote objects resembles the same as with local objects.
You can use @code{<-} and @code{on} which means that most programs
that were originally designed for local-only computing
naturally scale out to a networked environment.
@item
All this is done while mostly hiding the abstraction of the networked
protocol from the user.
@item
Object capability security is upheld.  A remote machine cannot make
use of any capability that has not been handed to it.
@item
Live object references between CapTP endpoints are incredibly cheap,
merely represented as integers on each side.  This keeps message sizes
small and efficient.
@item
CapTP provides (acyclic) distributed garbage collection.  Remote
machines can indicate when they no longer need an object and the
machine locally containing that reference can reclaim it if
appropriate.
@item
Promise pipelining means that messages can be sent to the resolution
of a promise before that resolution actually occurs.  Over the network
this is represented as a pipeline of messages.  This can reduce round
trips significantly, which is a big win.

To re-quote Mark S@. Miller:

@quotation
Machines grow faster and memories grow larger.  But the speed of light
is constant and New York is not getting any closer to Tokyo.
@end quotation
@item
CapTP is written independently of network transport abstractions.  It
can be run over local Unix domain sockets, an OpenSSL connection, Tor
Onion Services, or something custom.  CapTP operates under the
assumption of secure pairwise channels; the layer which provides the
pairwise channels is called ``MachineTP'' and can be written in a
variety of ways.  A simple MachineTP abstraction is provided, but
presently still requires some manual wiring.  In the future, even
simpler abstractions will be provided which cover most user needs.
@end itemize

All of these combine into an efficient and powerful protocol that,
most powerfully of all, the author of Goblins programs need not
think too much about.  This circumvents the usual years and years of
careful message structure coordination and standardization for many
kinds of distributed programs, which can simply be written as normal
Goblins programs rather than bespoke protocols.

Limitations of CapTP:

@itemize
@item
Goblins' CapTP provides @emph{acyclic} distributed garbage collection.
Cycles between servers are not automatically recognized.  Full
@uref{http://erights.org/history/original-e/dgc/, cycle-collecting
distributed garbage collection} has been written, but requires special
cooperation from the garbage collector that is not available in
Guile (or in most languages).
@item
Goblins' CapTP does not do anything about memory usage or resource
management on its own.  (Features for this will come as Spritely
sub-projects in the future.)
@item
While CapTP in theory routes capabilities to specific remote objects,
since the network is mutually suspicious one can't assume that the
remote end isn't conspiring some way to hand those capabilities to
other unexpected objects.  The right way to think about this
from an object capability perspective is that a remote misbehaving
machine is equivalent to a misbehaving object with the surface area of
the entire machine.
@item
@strong{TEMPORARY}: The API is heavily in flux.
@end itemize

@node Launching a Tor daemon for Goblins
@section Launching a Tor daemon for Goblins

In order for Goblins' Onion Service netlayer to work, it needs to talk
to a Tor daemon.  Goblins can set up and manage the Onion Services
itself once it connects to the Tor daemon.  So far it seems like
running a single Goblins Tor process ``as your user'' and letting
various Goblins processes connect to it is the easiest option.

Here's an example configuration file template (replace @code{<user>}
with your actual username):

@example
DataDirectory /home/<user>/.cache/goblins/tor/data/
SocksPort unix:/home/<user>/.cache/goblins/tor/tor-socks-sock RelaxDirModeCheck
ControlSocket unix:/home/<user>/.cache/goblins/tor/tor-control-sock RelaxDirModeCheck
Log notice file /home/<user>/.cache/goblins/tor/tor-log.txt@}|
@end example

Write this configuration to @file{~/.config/goblins/tor-config.txt}.

Create the necessary cache directory:

@example
mkdir -p ~/.cache/goblins/tor/data
@end example

Now launch the Tor daemon:

@example
tor -f ~/.config/goblins/tor-config.txt
@end example

@node Example Two Goblins programs chatting over CapTP via Tor
@section Example: Two Goblins programs chatting over CapTP via Tor

Before diving into API details, let's take a look at a couple of small
example programs to demonstrate that this stuff works.

Here's our client script named @file{captp-alice.scm}:

@lisp
(use-modules (fibers conditions)
             (fibers operations)
             (goblins)
             (goblins ocapn ids)
             (goblins ocapn captp)
             (goblins ocapn netlayer onion)
             (ice-9 match))

;; Create a CapTP router using Tor Onion Services.
(define machine-vat (spawn-vat))
(define-values (onion-netlayer private-key service-id)
  (with-vat machine-vat
    (new-onion-netlayer)))
(define mycapn
  (with-vat machine-vat
    (spawn-mycapn onion-netlayer)))

;; Convert the command-line argument into a sturdyref for Bob.
(define bob-uri
  (match (command-line)
    ((_ uri) uri)))
(define bob-sref (string->ocapn-id bob-uri))

;; Create a vat for Alice and enliven Bob.
(define a-vat (spawn-vat))
(define bob-vow
  (with-vat a-vat
    (<- mycapn 'enliven bob-sref)))

;; Send a message to Bob and print the response.
(define done? (make-condition))
(with-vat a-vat
  (on (<- bob-vow "Alice")
      (lambda (response)
        (format #t "Alice heard back: ~a" response)
        (signal-condition! done?))))

;; Wait until Alice has received a response before exiting.
(perform-operation (wait-operation done?))
@end lisp

Traditionally, Alice talks to Bob.  So, now we need Bob.  Save the
following server script as @file{captp-bob.scm}:

@lisp
(use-modules (fibers conditions)
             (fibers operations)
             (goblins)
             (goblins ocapn ids)
             (goblins ocapn captp)
             (goblins ocapn netlayer onion))

;; Bob is going to be a greeter that says hello to other people.
(define (^greeter _bcom our-name)
  (lambda (your-name)
    (format #f "Hello ~a, my name is ~a!" your-name our-name)))

;; Create Bob in their own vat.
(define b-vat (spawn-vat))
(define bob (with-vat b-vat (spawn ^greeter "Bob")))

;; Create a CapTP router using Tor Onion Services.
(define machine-vat (spawn-vat))
(define-values (onion-netlayer private-key service-id)
  (with-vat machine-vat
    (new-onion-netlayer)))
(define mycapn
  (with-vat machine-vat
    (spawn-mycapn onion-netlayer)))

;; Create a sturdyref for Bob and print it.
(define bob-sref
  (with-vat machine-vat
    ($ mycapn 'register bob 'onion)))
(format #t "Bob's sturdyref: ~a"
        (ocapn-id->string bob-sref))

;; Hold the process open indefinitely.
(perform-operation (wait-operation (make-condition)))
@end lisp

Okay, time to try it out!  Open up two terminals, and in the first one
run:

@example
guile captp-bob.scm
@end example

This will print out a captp ``sturdyref'' URI to the terminal.  Copy
it and paste it in the second terminal as the argument to @code{guile
captp-alice.scm}:

@example
guile captp-alice.scm <STURDYREF-GOES-HERE>
@end example

Tor Onion Services can take awhile to establish, but after a short
wait you should see:

@example
Alice heard back: Hello Alice, my name is Bob!
@end example

If you see the same output then your connection succeeded!  Yay! The
next sections will get into the details of how to use the CapTP API@.

@node Using the CapTP API
@section Using the CapTP API

Goblins uses an actor to communicate over CapTP@.  This actor can be
spawned using the @code{spawn-mycapn} procedure from the
@code{(goblins ocapn captp)} module:

@lisp
> (define mycapn (spawn-mycapn netlayer))
@end lisp

Where @code{netlayer} refers to a CapTP netlayer object.  See the next
section for details on available netlayers.

Local actors need to be registered so that they can receive messages
over CapTP@.  Registering an actor generates a CapTP identifier for
that actor:

@lisp
> (define alice (spawn ^greeter "Alice"))
> (define alice-captp-id ($ mycapn 'register alice 'onion))
@end lisp

To use this actor on another machine, that machine needs a
``sturdyref'' to the actor.  A sturdyref is a special URI that
uniquely identifies the actor on the network.  The
@code{ocapn-id->string} procedure from the @code{(goblins ocapn ids)}
module can generate such a URI:

@lisp
> (ocapn-id->string community-sref)
=> "ocapn://jv4arqqwntmxs3rljkx3ao5bucci37rxxb575vypovntyve76mmyf4ad.onion/s/nGxw4_p6s91IF-R5r6MMYsZzoByODX46MV7AWTApX5s"
@end lisp

To generate a Guile URI object instead of a string, use
@code{ocapn-id->uri} instead.

On the other machine, the sturdyref can be ``enlivened'' to create a
promise that, when resolved, yields a remote object:

@lisp
> (define vow ($ mycapn 'enliven "ocapn://..."))
> vow
=> #<local-promise>
> (on vow
      (lambda (obj)
        (display obj)
        (newline)))
; #<remote-object>
@end lisp

Sending messages to remote objects using @code{<-} looks the same as
sending messages to local objects.  The message will be transparently
sent over CapTP to the remote object!

@enumerate
@item
@anchor{Keeping a persistant sturdyref for an object}Keeping a persistant sturdyref for an object


When an object is registered, it is given a ``swiss num'' (object
specific part of the sturdyref). This can be extracted from the ocapn
id:

@lisp
> (define swiss-num (ocapn-sturdyref-swiss-num alice-captp-id))
> swiss-num
=> #vu8(156 108 112 227 250 122 179 221 72 23 228 121 175 163 12 98 198 115 160 28 142 13 126 58 49 94 192 89 48 41 95 155)
@end lisp

Often it is desirable to have an object have a persistant sturdyref
between sessions; to do this, you need to do two things:

@itemize
@item
Register the object at a specific swiss num
@item
Restore the netlayer (see the netlayer section)
@end itemize

To install the object at a given swiss num, get the nonce
registry. This is an object which holds all of the swiss nums to each
registered object in mycapn. The following is how this can be done:

@lisp
> (define registry ($ mycapn 'get-registry)) ;; nonce registry
> (define alice-captp-id ($ registry 'register alice swiss-num))
@end lisp
@end enumerate

@node Netlayers
@section Netlayers

@enumerate
@item
@anchor{Tor Onion Services}Tor Onion Services


Assuming a Tor daemon is running, a Goblins Onion netlayer can be
created using the @code{new-onion-netlayer} procedure from the
@code{(goblins ocapn netlayer onion)} module:

@lisp
> (define-values (onion-netlayer private-key service-id)
    (new-onion-netlayer))
@end lisp

That netlayer can then be used to spawn an OCapN router actor:

@lisp
> (define mycapn (spawn-mycapn onion-netlayer))
@end lisp

Each time @code{new-onion-netlayer} is called it generates a new
@code{private-key} and @code{service-id}.  If you wish for these to
persist, they need to be stored (securely, as anyone with them has
the capability to host at them) and call @code{restore-onion-netlayer}
providing them. An example of this is:

@lisp
> (define-values (onion-netlayer _private-key _service-id)
    (restore-onion-netlayer private-key service-id))
@end lisp
@end enumerate

